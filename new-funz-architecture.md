# Funz + Maven + OSGi

## Contexte
Passage des différents projets Funz en contexte compatible directement avec Prométhée: c'est-à-dire utilisation de maven pour packaging (*et upload maven central ?*) et génération des metadonnées nécessaires à OSGi (packages importés, exportés essentiellement)

## Projets Concernés
 - funz-profile
 - funz-core
 - funz-calculator 
 - funz-client
 
## Modifications générales/workflow

Le projet `funz-profile` est utilisé en tant que project parent maven: il décrit les comportements globaux de Maven et les différentes versions des plugins maven.

Tous les projets ont désormais un **pom.xml** pour la mavenisation.

Les tests de `funz-calculator` et les tests 'intégration' de `funz-client` sont déportés dans un **nouveau projet annexe pour le moment nommé `funz-test`**. En effet les tests de funz-calculator utilisent des classes de funz-client ce qui génère une dépendance cyclique qui ne plait pas du tout à Maven. Seuls quelques tests de funz-client (ceux qui y sont restés) sont de vrais tests unitaires utilisant JUnit, les autres passent par un mécanisme ad-hoc qui nécessite de démarrer des calculateurs. Ces tests plus complexes et plus longs ont plutôt intérêt d'être séparés des tests unitaires.

__L'API développeur basée sur ANT est conservée__ mais toute la partie compilation/gestion des dépendences est déléguée à Maven (appel maven depuis ANT).

## Modifications projet par projet

### Funz-profile

Déjà abordé, nouveau `pom.xml` centralisant les versions de plugins maven.

### Funz-core

En plus du nouveau pom.xml, des changements dans le code source ont dû être effectués. En effet, les packages `org.funz` et `org.funz.log` sont présents à la fois dans funz-core et dans funz-client. Cela pose de nombreux problèmes à OSGi qui ne sait pas dans quel projet allez chercher l'information. Il est possible de contourner le problème dans OSGi mais non sans effets de bords controlés.

Ainsi, le projet funz-core a été modifié et **toutes les classes précedemment dans les packages** `org.funz` **et** `org.funz.log` **sont désormais dans** `org.funz.core`. Cela concerne les classes suivantes:
 - Constants.java
 - LogCollector.java
 - LogConsole.java
 - LogFile.java
 - LogNull.java
 - LogTicToc.java
 - Protocol.java
 - XMLConstants.java

### Funz-calculator

Désormais, il n'y a plus de tests dans ce projet. De plus le code source a été adapté aux changements de package dans `funz-core` (mise à jour des imports).

Les tests de calculator peuvent être retrouvés dans le nouveau projet `funz-test`.

Pour récupérer les infos système, remplacement de l'utilisation de Sigar et de OperatingSystemMXBean par OSHI (directement compatible OSGi).

### Funz-client

Désormais, uniquement les *vrais* tests unitaires sont présents dans funz-client, les autres sont dans `funz-test`. Les tests conservés dans client sont ceux des packages:
 - `org.funz`
 - `org.funz.parameter`
 - `org.funz.script`
 
Le code source a également été adapté aux changements de package dans `funz-core` (mise à jour des imports).


## Nouveau projet de tests: *funz-test*

Ce projet vise à rassembler tous les tests fonctionnels de Funz.
Ces tests sont issus de calculator et client.

Il n'y a pas de vrai code source, uniquement des classes Java de test et de nombreuses ressources. 
Les classes java sont compilées par maven qui remplit le dossier `dist/lib` avec toutes les dépendances du projet.

Le code de lancement des tests est celui qui été présent dans client: parcourt tous les fichiers Test et lance le main.

Modification du script Funz.R pour prendre en compte le changement de package de Constants.
Modification du script Funz.py pour prendre en compte le changement de package de Constants.

## État actuel des développements

### Compilation / Exécution

Les trois projets (core, calculator, client) compilent correctement.

Je les ai testé en contexte OSGi/Prométhée, l'utilisation directe des .jar client, core et calculator fonctionne correctement.

### Tests

#### Funz-core
OK.

#### Funz-calculator
Plus de tests.

#### Funz-client
OK sauf le test `RenjinMathExpressionTest#testDecimal` qui ne passe pas chez moi. (*Configuration du sytème incorrecte?*).

#### Funz-test

Oulà ! C'est ici qu'il reste le plus de boulot pour faire marcher les tests.

##### Test Java

Timeout ? Batch bloqué ?

Erreurs dans le log, à l'origine du problème ?

```bash
Project directory /home/atrouche/.Funz/projects/branin.R already taken. Using /home/atrouche/.Funz/projects/branin.R_20200429-145222 instead.
[EXCEPTION]  Could not find variable x1,x2
Case spool dir /home/atrouche/.Funz/projects/branin.R_20200429-145222/spool/x1,x2=0.1,0.1 does not exists.
[EXCEPTION]  Could not find variable x1,x2
```


##### Test R
Je n'arrive pas à faire tourner sur mon ordi, problème d'installation de rJava+REngine impossible sur mon ordinateur (*R trop récent ?*)

##### Test Bash
Fonctionnent correctement.

| Nom              |       Résultat      |
|------------------|:-------------------:|
| DesignTest.sh    |  OK (3 tests) / 65s |
| RunDesignTest.sh | OK (6 tests) / 187s |
| RunTest.sh       |  OK (4 tests) / 41s |

##### Test CMD
Pas de dossier `src/test/cmd`. Mais je n'ai trouvé ce dossier nulle part dans les projets (même versions sur GitHub).
Est-ce que ce test a déjà fonctionné ?

##### Test Python
*Fonctionne après installation de py4j.*
OK, à part *RunDesignTest* qui timeout.

| Nom              |        Résultat        |
|------------------|:----------------------:|
| DesignTest.py    |   OK (1 test) / 246s   |
| RunDesignTest.py | Timeout: aucune sortie |
| RunTest.py       |   OK (5 tests) / 98 s  |

##### Test Issues
'OK'.

| Nom                        |       Résultat       |
|----------------------------|:--------------------:|
| BreakingCalculatorTest.sh  |       OK /47 s       |
| RBreakingCalculatorTest.sh | Failed: R marche pas |

## À propos de maven/osgi

Voici quelques informations de base sur le fonctionnement de maven+osgi.

Je passe les détails sur le fonctionnement de maven.
L'OSGisation de Funz se limite à quelques instructions dans le `pom.xml`.

### Le *maven-bundle-plugin*

L'utilisation de plugin maven va réaliser le nécessaire pour réaliser un jar compatible OSGi.
Sa version est fixée dans `funz-profile` (actuellement 4.2.1).
Il doit être présent dans la section `<build><plugins>` de chaque pom concerné.

### Le packaging *bundle*

Pour obtenir un jar étiqueté bundle OSGi, il est nécessaire de définir le packaging à bundle.
Cela soit être réalisé dans chaque pom.xml de la sorte:

```XML
<packaging>bundle</packaging>
```

### Les directives OSGi

Il s'agit là du principal réglage qui peut être amené à être modifié.

OSGi a besoin de savoir quels packages le bundle a besoin (cette information peut-être calculée automatiquement en utilisant les dépendances Maven, si les dépendences listent bien tout le nécessaire, i.e. pas d'introspection ou autre). Mais également et surtout quels packages les bundle rend disponible aux autres bundles. -
Cette information ne peut être calculée automatiquement car certains packages sont peut-être à garder interne au bundle.

Afin d'indiquer ces informations, il est nécessaire de paramétrer correctement le *bundle-plugin* dans le pom.xml. Prenons l'exemple de funz-calculator:

```XML
<plugin>
	<groupId>org.apache.felix</groupId>
	<artifactId>maven-bundle-plugin</artifactId>
	<configuration>
		<instructions>
			<Bundle-SymbolicName>${project.groupId}.${project.artifactId}</Bundle-SymbolicName>
			<Export-Package>
				org.funz.calculator;version=${project.version},
				org.funz.calculator.network;version=${project.version},
				org.funz.calculator.plugin;version=${project.version},
				org.funz.codes;version=${project.version},
			</Export-Package>
		</instructions>
	</configuration>
</plugin>
```

On peut remarquer 2 instructions principales:
 - `Bundle-SymbolicName` permet d'indiquer à OSGi comment construire un nom de bundle unique: ici en associant le groupId *org.funz* à l'artifactId *funz-calculator*, on obtient un nom suffisamment unique de *org.funz.funz-calculator*.
 - `Export-Package` permet d'indiquer la liste des packages à exporter pour le bundle: ici on a décidé de tout exporter.
 
Ces deux instructions suffisent pour que le *bundle-plugin* annote correctement le jar pour en faire un bundle OSGi valide.

Il est à noter que d'autres instructions sont possibles dans ce contexte, voici quelques unes qui pourrait être utiles:
 - `Import-Package` permet de definir la liste des packages à importer pour que le bundle fonctionne. Ne pas l'indiquer revient à indiquer `<Import-Package>*</Import-Package>` qui indique de marquer en packages importés tous les packages des dépendances maven. Il peut être nécessaire de l'indiquer si on a besoin de packages pas détectés par maven (par exemple pour de l'introspection) de la sorte: `nom.du.package,*`
 - `Private-Package` permet d'indiquer qu'un package n'a pas vocation à être exporté. Attention, si un package est indiqué à la fois dans *Private* et *Export* il sera exporté: le *Export* prend le dessus.




